<p align="center">
  <a href="https://gitlab.com/alexhoulton/homebridge-meraki-mt-sensor"><img src="https://gitlab.com/alexhoulton/homebridge-meraki-mt-sensor/-/raw/main/graphics/meraki.png" height="140"></a>
</p>

<span align="center">

# Homebridge Meraki MT Plugin
[![npm](https://badgen.net/npm/dt/homebridge-meraki-mt-sensor?color=purple)](https://www.npmjs.com/package/homebridge-meraki-mt-sensor) [![npm](https://badgen.net/npm/v/homebridge-meraki-mt-sensor?color=purple)](https://www.npmjs.com/package/homebridge-meraki-mt-sensor)

Homebridge plugin for Meraki MT devices.

</span>

## Info
This plugin enables you to monitor Meraki MT sensor data in Homebridge, and thus in the Apple Home app.

## Installation
1. Follow the step-by-step instructions on the [Homebridge Wiki](https://github.com/homebridge/homebridge/wiki) for how to install Homebridge.
2. Follow the step-by-step instructions on the [Homebridge Config UI X](https://github.com/oznu/homebridge-config-ui-x/wiki) for how to install Homebridge Config UI X.
3. Install homebridge-meraki-mt-sensor using: `npm install -g homebridge-meraki-mt-sensor` or search for `meraki mt` in Config UI X.

## Configuration
Use [Homebridge Config UI X](https://github.com/oznu/homebridge-config-ui-x) to configure the plugin (strongly recomended), or update your configuration file manually. See `sample-config.json` in this repository for a sample or add the bottom example to Your config.json file.

## Whats new:
https://gitlab.com/alexhoulton/homebridge-meraki-mt-sensor/-/raw/main/CHANGELOG.md

## Development
- Pull request and help in development highly appreciated.